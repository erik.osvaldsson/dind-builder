FROM docker

# Set up buildx
COPY --from=docker/buildx-bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx

# Add git
RUN apk add --update --no-cache git

# Add python/pip
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 py3-pip py3-setuptools && ln -sf python3 /usr/bin/python
RUN pip3 install --no-cache --upgrade --break-system-packages pipenv

# Add skopeo
RUN apk add --update --no-cache skopeo
