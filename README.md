# dind-builder

A Docker image for building using docker-in-docker configurations.

Includes
* docker
* buildx
* Python 3
* skopeo
